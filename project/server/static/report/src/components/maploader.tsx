import React from 'react'
import { RenderAfterNavermapsLoaded } from 'react-naver-maps'
import { MapView } from './mapview'
import { naverClientId } from '../app'

export const MapLoader: React.FC = () => {
  return (
    <RenderAfterNavermapsLoaded
      ncpClientId={naverClientId} // 자신의 네이버 계정에서 발급받은 Client ID
      error={<p>Maps Load Error</p>}
      loading={<p>Maps Loading...</p>}
    >
      <React.Suspense fallback={<div>Loading...</div>}>
        <MapView></MapView>
      </React.Suspense>
    </RenderAfterNavermapsLoaded>
  );
}
