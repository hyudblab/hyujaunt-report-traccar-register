import React, { SyntheticEvent } from 'react'
import { useRecoilValue } from 'recoil'
import { NaverMap, Marker } from 'react-naver-maps'
import { Stop, stops, questions } from '../store'
import { getPathOnAppBaseUrl } from '../helpers'
import 'react-bulma-components/dist/react-bulma-components.min.css';
import { Button } from 'react-bulma-components'

interface Answer {
  qid: number
  value: string
  timestamp: Date
}

export const ReportView: React.FC<{ closeReportView: () => void, reportingItem: Stop }> = ({ closeReportView, reportingItem }) => {
  const questionsValue = useRecoilValue(questions)
  const stopsState = useRecoilValue(stops)

  const saveForm = (ev: SyntheticEvent) => {
    ev.preventDefault()
    console.log("SUBMITED", reportingItem)
    const report = {
      deviceid: reportingItem.deviceid,
      stopId: reportingItem.index,
      lastAnswer: new Date(),
      answers: [] as Answer[]
    }
    for (const question of questionsValue) {
      const aid = "a" + question.index
      if (question.type === 'choice') {
        const answerElements = document.querySelectorAll<HTMLInputElement>(`input[name="${aid}"]:checked`)
        for (const answerElement of Array.from(answerElements)) {
          const value = answerElement.value
          const answer = {
            'qid': question.index,
            'value': value,
            'timestamp': new Date()
          }
          report.answers.push(answer)
        }
      }
      else {
        const answerElement = document.querySelector<HTMLInputElement>(`input[name="${aid}"]`)
        const answer = {
          'qid': question.index,
          'value': answerElement!.value,
          'timestamp': new Date()
        }
        report.answers.push(answer)
      }
    }
    console.log(report)
    const url = getPathOnAppBaseUrl("/questionnaire")
    fetch(url, {
      method: "POST",
      cache: "no-cache",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(report)
    }).then(res => {
      console.log("FETCH", res)
      if (res.ok) closeReportView();
      else {
        alert("[에러] 저장이 올바르게 되지 않았음! 새로고침 후 다시 시도하거나 관리자에게 문의해주세요.")
        closeReportView()
      }
    }).catch(reason => {
      console.error(reason)
      alert("[에러] 서버로 보내기 실패! 저장이 되지 않았음! 새로고침 후 다시 시도하거나 관리자에게 문의해주세요.")
      closeReportView()
    })
  }

  return (
    <div className="ReportModal">
      <NaverMap className="ReportThumbMap" id={'maps-on-report-survey'} mapDivid={'maps-on-report-survey'}
        center={{ lat: reportingItem.lat, lng: reportingItem.lng }}
      >
        <Marker position={{ lat: reportingItem.lat, lng: reportingItem.lng }} />
      </NaverMap>
      <h1>머문 위치에 대한 간단 설문</h1>
      <h3>{reportingItem.datetime.toLocaleString()} ~ {reportingItem.leavingDatetime.toLocaleString()}</h3>
      <h3>주소: {reportingItem.address}</h3>
      <h5>({reportingItem.lat}, {reportingItem.lng})</h5>
      <form onSubmit={(ev) => saveForm(ev)}>
        {questionsValue.map((item, index) => {
          const stopId = reportingItem.index
          const currentAnswers: any[] = stopsState[stopId].answer !== null ? stopsState[stopId].answer["answers"] : []
          const multiAnswer = currentAnswers.filter(e => e.qid === index)
          const answer = multiAnswer.length > 0 ? multiAnswer[0] : false
          console.log("둘이다르지", answer, multiAnswer)
          switch (item.type) {
            case "choice":
              if (item.isMultiple) {
                console.log("체크박스 객관식", answer, item.choices)
                return (<div key={index} id={"q" + index}>
                  <p>{index + 1}. {item.text}</p>
                  {item.choices?.map((choice, cIndex) => (
                    <p key={index + "-" + cIndex}>
                      <input type='checkbox' value={choice.mappedValue} name={"a" + index}
                        defaultChecked={!!(multiAnswer.find(answer => answer.value === choice.mappedValue))} />
                      <label >{choice.prompt}</label>
                    </p>
                  ))}
                </div>)
              }
              return (<div key={index} id={"q" + index}>
                <p>{index + 1}. {item.text}</p>
                {item.choices?.map((choice, cIndex) => {
                  const flag = answer ? (answer.value === choice.mappedValue || parseInt(answer.value) === choice.mappedValue) : false
                  return (
                    <p key={index + "-" + cIndex}>
                      <input type='radio' value={choice.mappedValue} name={"a" + index} defaultChecked={flag} />
                      <label>{choice.prompt}</label>
                    </p>
                  )
                })}
              </div>)
            case "textEntry":
              const text = answer ? answer.value : ""
              return (<div key={index} id={"q" + index}>
                <p>{index + 1}. {item.text}</p>
                <input type="text" name={"a" + index} defaultValue={text}></input>
              </div>)
          }
        })}
        <div className="field is-grouped">
          <Button color="success" type="submit">저장 후 닫기</Button>
          <Button color="danger" onClick={closeReportView}>저장 하지 않고 닫기</Button>
        </div>
      </form>
    </div>
  )
}
