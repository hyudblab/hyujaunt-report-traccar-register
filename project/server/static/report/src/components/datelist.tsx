import React, { ChangeEvent, SyntheticEvent, useState } from 'react'
import Modal from 'react-modal'
import { useRecoilState, useRecoilValue } from 'recoil'
import { filterStops, filterStopsValue, Stop, DEFAULT_STOP } from '../store'
import { convertLocalTime } from '../helpers'
import { ReportView } from './ReportView'


export const DateList: React.FC = () => {
  const stopsState = useRecoilValue(filterStops)
  const [filterStopsState, filterList] = useRecoilState(filterStopsValue)
  const [dateFrom, setDateFrom] = useState(filterStopsState.datetimeFrom)
  const [dateTo, setDateTo] = useState(filterStopsState.datetimeTo)
  const [addressFilter, setAddressFilter] = useState("")
  const [reportOpen, setReportOpen] = useState(false)
  const [reportingItem, setReportingItem] = useState<Stop>(DEFAULT_STOP)

  const addressFilterChange = (ev: ChangeEvent<HTMLInputElement>) => {
    // TODO: 주소 필터링 시, 한글 한 글자 완성 event 를 emulation 해야함
    // https://gist.github.com/sooop/4958873#file-hangulsound-js-L2
    const { value } = ev.target
    const before = addressFilter.length
    const after = value.length
    // 길이로 확인하는 건 일단 실패. 마지막 글자를 거르면 되지 않을까?
    // if (before !== 0 && before !== after) {
    //   console.log("한글자 완성", value)
    // }
    setAddressFilter(value)
    // return filterList({
    //   ...filterStopsState,
    //   address: value
    // })
  }

  const dateFilterChange = (ev: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = ev.target
    if (name === 'datetimeFrom') {
      setDateFrom(new Date(value))
    } else {
      setDateTo(new Date(value))
    }
  }

  const dateFilterSubmit = (ev: SyntheticEvent<HTMLFormElement>) => {
    ev.preventDefault()
    const target = ev.target as typeof ev.target & {
      datetimeFrom: { value: string }
      datetimeTo: { value: string }
    }
    const { datetimeFrom, datetimeTo } = target
    return filterList({
      ...filterStopsState,
      datetimeFrom: new Date(datetimeFrom.value),
      datetimeTo: new Date(datetimeTo.value)
    })
  }

  const linkToReportPage = (ev: SyntheticEvent, item: Stop) => {
    setReportOpen(true)
    setReportingItem(item)
  }

  const closeReportView = () => {
    console.log("CLOSE!")
    setReportOpen(false)
  }

  return (
    <div className="DateListWrapper">
      <div className="DateListHeader">
        <h3>위치정보 리스트</h3>
        <div>
          <form onSubmit={e => dateFilterSubmit(e)}>
            <label>진입시간</label>
            <input
              type="datetime-local" name="datetimeFrom"
              value={convertLocalTime(dateFrom)}
              onChange={e => dateFilterChange(e)}></input>
            <input type="datetime-local" name="datetimeTo"
              value={convertLocalTime(dateTo)}
              onChange={e => dateFilterChange(e)}></input>
            <input type="submit" value="적용"></input>
          </form>
        </div>
      </div>
      <div className="DateList" >
        {stopsState.map((item, index) => {
          if (!!!item.answer) {
            return (
              <div key={index} className="stopItem" onClick={e => linkToReportPage(e, item)}>
                <div>위치: {item.lat}, {item.lng}</div>
                <div>진입시간: {item.datetime.toLocaleString()}</div>
                <div>떠난시간: {item.leavingDatetime.toLocaleString()}</div>
                <div>주소: {item.address} </div>
              </div>
            )
          }
          else {
            return (
              <div key={index} className="stopItem hasAnswer" onClick={e => linkToReportPage(e, item)}>
                <div>위치: {item.lat}, {item.lng}</div>
                <div>진입시간: {item.datetime.toLocaleString()}</div>
                <div>떠난시간: {item.leavingDatetime.toLocaleString()}</div>
                <div>주소: {item.address} </div>
                <div className="notice">이미 답변을 저장한 적 있는 위치</div>
              </div>
            )
          }
        })}
      </div>
      <div>
        <Modal isOpen={reportOpen} ariaHideApp={false}>
          <ReportView closeReportView={closeReportView} reportingItem={reportingItem} />
        </Modal>
      </div>
    </div>
  )
}
