import React, { useState } from 'react'
import { useRecoilValue } from 'recoil'
import { filterStops, Stop, DEFAULT_STOP } from '../store'
import { NaverMap, Marker } from 'react-naver-maps'

export const MapView: React.FC = () => {
  const stopsState = useRecoilValue(filterStops)
  // const [mapCenter, setMapCenter] = useState({ lat: 37.554722, lng: 126.970833 })
  const [mapCenter, setMapCenter] = useState(
    { lat: DEFAULT_STOP.lat, lng: DEFAULT_STOP.lng }
  )

  const handleMarkerClickEvent = (ev: any, item: Stop) => {
    setMapCenter({ lat: item.lat, lng: item.lng })
  }

  return (
    <NaverMap
      id={'main-maps'}
      mapDivid={'maps-getting-started-uncontrolled'} // default: react-naver-map
      style={{
        width: '100%', // 네이버지도 가로 길이
        height: '100vh' // 네이버지도 세로 길이
      }}
      center={mapCenter} // 지도 초기 위치
      defaultZoom={13} // 지도 초기 확대 배율
    >
      {stopsState.map((item, index) => (
        <Marker key={index}
          position={{ lat: item.lat, lng: item.lng }}
          onClick={ev => handleMarkerClickEvent(ev, item)}
        />
      ))}


    </NaverMap>
  );
}
