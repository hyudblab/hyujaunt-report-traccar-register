import * as React from 'react'
import { useState, useEffect } from 'react';
import Sidebar from 'react-sidebar'
import { DateList } from './components/datelist'
import { MapLoader } from './components/maploader'
import { config } from 'dotenv'
import './App.css'

config()
export const naverClientId: string = process.env.NAVER_CLIENT_ID || ""

interface State {
  IsSidebarOpen: boolean
}

const App: React.FC = () => {
  const [sidebarOptions, setSidebarOptions] = useState<State>({
    IsSidebarOpen: false
  })
  const [isDesktop, setDesktop] = useState(window.innerWidth > 1450);

  const onSetSidebarOpen = () => {
    setSidebarOptions({
      IsSidebarOpen: !sidebarOptions.IsSidebarOpen
    })
  }
  const updateMedia = () => {
    setDesktop(window.innerWidth > 1450);
  };

  useEffect(() => {
    window.addEventListener("resize", updateMedia);
    return () => window.removeEventListener("resize", updateMedia);
  })

  const hiddenMapStyle = {
    display: "none"
  }

  return (
    <div>
      {isDesktop ? (
        <Sidebar
          sidebar={
            <React.Suspense fallback={<div>Loading...</div>}>
              <DateList />
            </React.Suspense >
          }
          docked={true}
          open={sidebarOptions.IsSidebarOpen}
          onSetOpen={onSetSidebarOpen}
          styles={{ sidebar: { background: "white", width: '20%', zIndex: "0" } }}
        >
          <MapLoader />
        </Sidebar>
      ) : (
          <div>
            <React.Suspense fallback={<div>Loading...</div>}>
              <DateList />
            </React.Suspense >
            <div style={hiddenMapStyle}><MapLoader /></div>
          </div>
        )
      }
    </div>
  )
}

export default App;
