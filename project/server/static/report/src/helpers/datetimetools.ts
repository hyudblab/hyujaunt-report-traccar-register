export function convertLocalTime(date: Date) {
  const offset = new Date().getTimezoneOffset()
  const localDateInMillSeconds = date.getTime() - offset * 1000 * 60
  const localDate = new Date(localDateInMillSeconds)
  const localDateFormat = localDate.toISOString().substr(0, localDate.toISOString().length - 8)
  return localDateFormat
}