export function getPathOnAppBaseUrl(url: string) {
  return window.location.href.split('?')[0].replace(/\/$/, "") + url
}
