/// <reference types="navermaps" />

declare module 'react-naver-maps' {
  interface NaverMapProps {
    navermaps?: ReactPropTypes.object.isRequired,
    registerEventInstance?: ReactPropTypes.func.isRequired,
    id?: ReactPropTypes.string,
    style?: ReactPropTypes.object,
    className?: ReactPropTypes.string,
    children?: ReactPropTypes.node,
    events?: string[],
    disableDoubleClickZoom?: ReactPropTypes.bool,
    disableDoubleTapZoom?: ReactPropTypes.bool,
    disableKineticPan?: ReactPropTypes.bool,
    disableTwoFingerTapZoom?: ReactPropTypes.bool,
    draggable?: ReactPropTypes.bool,
    keyboardShortcuts?: ReactPropTypes.bool,
    logoControl?: ReactPropTypes.bool,
    mapDataControl?: ReactPropTypes.bool,
    mapTypeControl?: ReactPropTypes.bool,
    maxBounds?: ReactPropTypes.object,
    pinchZoom?: ReactPropTypes.bool,
    resizeOrigin?: ReactPropTypes.number,
    scaleControl?: ReactPropTypes.bool,
    scrollWheel?: ReactPropTypes.bool,
    overlayZoomEffect?: ReactPropTypes.string,
    tileSpare?: ReactPropTypes.number,
    tileTransition?: ReactPropTypes.bool,
    zoomControl?: ReactPropTypes.bool,
    zoomOrigin?: ReactPropTypes.object,
    mapTypeId?: ReactPropTypes.number,
    size?: ReactPropTypes.object,
    bounds?: ReactPropTypes.object,
    center?: ReactPropTypes.object,
    centerPoint?: ReactPropTypes.object,
    zoom?: ReactPropTypes.number,
    transitionOptions?: ReactPropTypes.object,
    defaultCenter?: any,
    defaultZoom?: any,
    mapDivid?: string
  }
  interface RenderAfterNavermapsLoadedProps {
    loading: ReactPropTypes.node = null
    error: ReactPropTypes.node = null
    ncpClientId: string
    submodules?: string[]
    children?: ReactPropTypes.node
  }
  interface MarkerProps {
    position: any
    animation?: number
    clickable: bool = true
    cursor: string = "pointer"
    draggable: bool = false
    events: string[] = ['animation_changed', 'click', 'clickable_changed', 'dblclick', 'draggable_changed', 'icon_changed', 'mousedown', 'mouseout', 'mouseover', 'mouseup', 'position_changed', 'shape_changed', 'title_changed', 'visible_changed', 'zIndex_changed',]
    icon?: any
    navermaps?: any
    shape?: any
    title: string = 'null'
    visible: boll = true
    zIndex?: number
  }
  class NaverMap extends React.PureComponent<NaverMapProps> {
  }
  class RenderAfterNavermapsLoaded extends React.PureComponent<RenderAfterNavermapsLoadedProps> {
  }
  class Marker extends React.Component<MarkerProps> { }
}
