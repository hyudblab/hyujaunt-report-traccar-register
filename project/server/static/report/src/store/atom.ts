import { atom } from 'recoil'
import has from 'has'
import { getPathOnAppBaseUrl } from '../helpers'

export interface Stop {
  index: number
  lat: number
  lng: number
  deviceid: number
  datetime: Date
  leavingDatetime: Date
  address: string
  answer: any
}

export const DEFAULT_STOP: Stop = {
  index: -1,
  lat: 37.5581214,
  lng: 127.0430032,
  deviceid: -1,
  datetime: new Date(),
  leavingDatetime: new Date(),
  address: "서울시 성동구 왕십리로 222",
  answer: {}
}

export interface StopFilters {
  datetimeFrom: Date
  datetimeTo: Date
  address: string
  radius?: number
}

export const stops = atom<Stop[]>({
  key: "stops",
  default:
    (async () => {
      try {
        const url = getPathOnAppBaseUrl("/stops")
        const res = await fetch(url)
        const stopsData = await res.json()
        const stops: Stop[] = stopsData.map((item: any, index: number) => (
          {
            index: index,
            lat: item.lat,
            lng: item.lng,
            deviceid: item.uid,
            datetime: new Date(item.datetime),
            leavingDatetime: new Date(item.leaving_datetime),
            address: item.address,
            answer: item.answer
          }
        ))
        return stops
      }
      catch (err) {
        throw err
      }
    })()
})

export const filterStopsValue = atom<StopFilters>({
  key: "filterStopsValue",
  default: {
    datetimeFrom: (() => {
      let day = new Date()
      day.setDate(day.getDate() - 7)
      return day
    })(),
    datetimeTo: new Date(),
    address: ""
  } //values: all or filter
});

interface Choice {
  index: number
  mappedValue: string | number
  prompt: string
}


interface Question {
  index: number
  isMultiple: boolean
  shuffle: boolean
  type: string
  text: string
  choices?: Choice[]
}

export const questions = atom<Question[]>({
  key: "questions",
  default:
    (async () => {
      // TODO: Endpoint query
      try {
        const url = getPathOnAppBaseUrl("/questionnaire")
        const res = await fetch(url)
        const questionData = await res.json()
        const questions: Question[] = questionData.map((item: any, index: number) => (
          {
            index: index,
            isMultiple: item.isMultiple,
            shuffle: item.shuffle,
            type: item.type,
            text: item.text,
            choices: has(item, 'choices') ? item.choices : undefined
          }
        ))
        return questions
      }
      catch (err) {
        throw err
      }
    })()
})
