
import { selector } from 'recoil'
import { Stop, stops, filterStopsValue } from '.'

export const filterStopsServer = selector<Stop[]>({
  key: 'filterStops',
  get: async ({ get }) => {
    const filterStopsValueState = get(filterStopsValue)

    const sample = await fetch('/report/sample').then(res => res.json())
    console.log(sample)
    console.log("데이트", new Date(sample[0].datetime))
    let filtered: Stop[] = sample.filter((stop: Stop) => {
      stop.datetime >= filterStopsValueState.datetimeFrom &&
        stop.datetime < filterStopsValueState.datetimeTo
    })
    if (filterStopsValueState.address) {
      return filtered.filter(stop =>
        stop.address && stop.address.includes(filterStopsValueState.address) && stop
      )
    }
    return filtered
  }
})

export const filterStops = selector<Stop[]>({
  key: 'filterStopsServer',
  get: async ({ get }) => {
    const stopsState = get(stops)
    const filterStopsValueState = get(filterStopsValue)

    let filtered = stopsState.filter(stop => (
      stop.datetime >= filterStopsValueState.datetimeFrom &&
      stop.datetime < filterStopsValueState.datetimeTo
    ))

    if (filterStopsValueState.address) {
      return filtered.filter(stop =>
        stop.address && stop.address.includes(filterStopsValueState.address) && stop
      )
    }
    return filtered
  }
})
