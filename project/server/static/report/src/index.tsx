import * as React from "react";
import * as ReactDOM from "react-dom";
import { RecoilRoot } from "recoil";
import App from './app';

ReactDOM.render(<RecoilRoot>
  <App />
</RecoilRoot>, document.getElementById('root'));
