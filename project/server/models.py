# coding: utf-8
from sqlalchemy.schema import FetchedValue
from flask_login import UserMixin
from . import db


class User(UserMixin, db.Model):
    __tablename__ = "User"

    id = db.Column(db.Integer, autoincrement=True, primary_key=True, server_default=FetchedValue(),)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100), nullable=False)
    name = db.Column(db.String(100), nullable=False)
