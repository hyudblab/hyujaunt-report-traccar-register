# init.py
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_pymongo import PyMongo
from .connections.TraccarConnection import TraccarConnection
from .connections.TraccarAPI import TraccarAPI
from .config import current_config, get_config

# init SQLAlchemy so we can use it later in our models
db = SQLAlchemy()
mongo = PyMongo()
traccar_api = TraccarAPI()
navermaps_client = {"id": "", "secret": ""}


def create_app():
    base_url = get_config("urlPrefix")
    if base_url is None:
        base_url = ""
    # Flask server config
    app = Flask(__name__, static_url_path=base_url, static_folder="static", template_folder="templates",)
    app.url_map.strict_slashes = False

    print("SERVER CONFIGURATRION: ", current_config)
    app.config.from_object(current_config)

    # pg config
    db.init_app(app)

    # mg config
    mongo.init_app(app)

    login_manager = LoginManager()
    login_manager.login_view = "auth.login"
    login_manager.init_app(app)

    from .models import User

    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return User.query.get(int(user_id))

    navermaps_client["id"] = app.config.get("NAVER_MAPS_CLIENT")["id"]
    navermaps_client["secret"] = app.config.get("NAVER_MAPS_CLIENT")["secret"]

    # Traccar Hello
    with app.app_context():
        traccar = TraccarConnection(get_config("traccar"))
        server_info = traccar.GET("server")
        if not server_info:
            print("No Traccar Connection when server starting")
            raise Exception
        traccar_api.traccar = traccar
        print("Traccar Connection -- OK")
        try:
            db.engine.execute("SELECT 1")
            db.create_all()
            print("Postgres Connection -- OK")
        except Exception as err:
            print("Cannot DB Connection", err)
            raise

    from .main import main as main_blueprint
    from .auth import auth as auth_blueprint
    from .fcm import fcm as fcm_blueprint
    from .report import report as report_blueprint

    app.register_blueprint(main_blueprint, url_prefix=base_url)
    app.register_blueprint(auth_blueprint, url_prefix=base_url)
    app.register_blueprint(fcm_blueprint, url_prefix=base_url + "/fcm")
    app.register_blueprint(report_blueprint, url_prefix=base_url + "/report")

    # from .traccar import traccar as traccar_blueprint

    # app.register_blueprint(traccar_blueprint)

    # from .dashboard import dashboard as dashboard_blueprint

    # app.register_blueprint(dashboard_blueprint, url_prefix="/dashboard")
    return app
