from server.helpers import parse_utc_date_as_formatted
from . import traccar_api
from flask import Blueprint, render_template, request, redirect, url_for, current_app
from flask_login import login_required, current_user
import requests
from pprint import pprint

main = Blueprint("main", __name__)


@main.route("/")
def index():
    return render_template("main.html")


@main.route("/info")
def info():
    tab_id = request.args.get("tab_id")
    if not (tab_id and int(tab_id) in [0, 1, 2]):
        tab_id = 0
    return render_template(f"info/info-{tab_id}.html")


@main.route("/enlist")
@login_required
def enlist():
    devices = []
    for device in traccar_api.fetch_devices():
        if current_user.id == device["attributes"].get("userId"):
            d = {
                "id": device["id"],
                "name": device["name"],
                "phone": device["phone"],
                "uniqueId": device["uniqueId"],
                "lastUpdate": parse_utc_date_as_formatted(device["lastUpdate"])
                if device.get("lastUpdate")
                else "최근 위치 정보 없음",
                "status": device["status"],
            }
            devices.append(d)
    return render_template("enlist.html", name=current_user.name, vid=current_user.id, devices=devices)


@main.route("/enroll", methods=["POST"])
@login_required
def enroll():
    name = request.form.get("name")
    phone = request.form.get("phone")
    unique_id = traccar_api.generate_uniqueId(phone)

    vid = current_user.id
    vname = current_user.name

    data = {
        "name": name,
        "uniqueId": unique_id,
        "phone": phone,
        "attributes": {"userId": vid, "userName": vname},
    }
    try:
        status = traccar_api.POST("devices", data)
        if status == "OK":
            return redirect(url_for("main.enlist"))
    except requests.exceptions.ConnectionError:
        print("Connection refused")
    return render_template("error.html", msg="Traccar Server: Enlist Failed")


@main.route("/resources")
def resources():
    return render_template("resources.html")


@main.route("/fntest")
def fn_test():
    links = []
    for rule in current_app.url_map.iter_rules():
        links.append(rule)
    pprint(links)
    return "test"
