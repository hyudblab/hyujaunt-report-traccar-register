from typing import Any, List
from .connections.TraccarAPI import TcDevice
from flask import Blueprint, request, render_template, redirect, url_for, jsonify, flash
from flask_login import login_required, current_user
from datetime import datetime
import json
from pyfcm import FCMNotification
from . import traccar_api
from .config import get_config

fcm = Blueprint("fcm", __name__)

push_service = FCMNotification(api_key=get_config("fcm").get("apikey"))

registration_ids = set()


@fcm.route("/")
@login_required
def index():
    devices_to_view = []
    devices = traccar_api.fetch_devices()
    for device in devices:
        attributes = device["attributes"]
        if current_user.name == "gs" or (attributes.get("userId") and current_user.id == attributes["userId"]):
            # For Korean Time Format
            d = {"name": device["name"], "uniqueId": device["uniqueId"], "token": device["attributes"].get("token")}
            devices_to_view.append(d)

    return render_template("fcm.html", name=current_user.name, vid=current_user.id, devices=devices_to_view)


@fcm.route("/fire")
@login_required
def fire():
    uids = request.args.getlist("uniqueId")
    print("===== FIRING MESSAGE TO ======")
    print("uids: ", uids)
    devices = traccar_api.fetch_devices(uids=uids)
    for device in devices:
        attributes = device["attributes"]
        if attributes.get("token"):
            print(f"{datetime.now()} firing to: {device['name']}, {device['uniqueId']}")
            data = {"title": "위치 리포트 요청", "body": f"{device['name']}님. 오늘 다니신 곳에 대해 간단한 설문을 작성해주세요"}
            send_single_message(attributes["token"], data)
    return redirect(url_for("fcm.index"))


@fcm.route("/fire-message")
@login_required
def fire_message():
    uids = request.args.getlist("uniqueId")
    message = request.args.get("message")
    print("===== FIRING MESSAGE ALL ======")
    devices: List[TcDevice] = traccar_api.fetch_devices() if not uids or len(uids) == 0 else traccar_api.fetch_devices(
        uids=uids
    )
    for device in devices:
        attributes = json.loads(device["attributes"])
        if attributes.get("token"):
            data = {"title": "실험 안내", "body": message}
            send_single_message(attributes["token"], data)
    return redirect(url_for("fcm.index"))


@fcm.route("/token", methods=["POST"])
def token():
    unique_id = request.form.get("uniqueId")
    token = request.form.get("token")
    model = request.form.get("model")
    carrier = request.form.get("carrier")
    print("Request", unique_id, token)

    params = {"uniqueId": unique_id}
    res = traccar_api.GET("devices", params=params)
    print("TOKEN RES", type(res), res)
    device = res[0]
    print("GET", device)

    # PUT token
    device["attributes"]["token"] = token
    device["attributes"]["model"] = model
    device["attributes"]["carrier"] = carrier

    status = traccar_api.PUT(f'devices/{device["id"]}', device)
    return jsonify({"uniqueId": unique_id, "status": status})


def send_single_message(token: str, data: Any):
    try:
        result = push_service.notify_single_device(registration_id=token, data_message=data)
        print(result)
        flash("성공!:" + json.dumps(result))
    except Exception as e:
        print("message could not sent!: ", e)
