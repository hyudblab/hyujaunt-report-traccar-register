from flask import Blueprint, render_template, request
from flask_login import login_required, current_user
from . import traccar_api

# from .main import fetch_devices

import json
import os

traccar = Blueprint(
    "traccar", __name__, static_folder="static", static_url_path="/static", template_folder="client/build"
)


@traccar.route("/device/", defaults={"path": ""})
@traccar.route("/device/<path:path>")
def device_report(path):
    pass_list = ["1878", "0381", "08471", "2948", "5080", "7005", "5684"]
    if path == "@test" or path in pass_list:
        return render_template("index.html")
    token = request.headers.get("Authorization")
    print("token:\n", token)
    uid = []
    uid.append(path)
    print(uid)
    devices = traccar_api.fetch_devices(uids=uid)
    print(devices)

    if len(devices) != 1:
        return render_template("error.html", msg=f"No such device {path}", msg_body="장치를 찾을 수 없습니다.")
    device = devices[0]
    attributes = json.loads(device.attributes)

    print("os", os.environ.get("FLASK_DEBUG"))
    if os.environ.get("FLASK_DEBUG") == "1":
        return render_template("index.html")
    # token
    if token and token == attributes["token"]:
        return render_template("index.html")
    return render_template(
        "error.html", msg="Invalid Token", msg_body="Report를 통해 접근하세요. 그래도 문제가 발생한다면 클라이언트의 서비스를 다시 시작해보세요."
    )


@traccar.route("/debug/device/", defaults={"path": ""})
@traccar.route("/debug/device/<path:path>")
@login_required
def device_report_debug(path):
    if current_user.name == "gs":
        return render_template("index.html")
    return render_template("error.html", msg="Unauthorized")
