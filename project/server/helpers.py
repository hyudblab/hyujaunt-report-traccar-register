from datetime import datetime
from typing import Any, Union
from dateutil import tz
from dateutil.parser import parse
import requests
from . import navermaps_client, traccar_api
from functools import reduce


def parse_utc_date(date: Union[datetime, str], dest_tz="Asia/Seoul") -> datetime:
    return parse(date).astimezone(tz.gettz(dest_tz)) if isinstance(date, str) else date.astimezone(tz.gettz(dest_tz))


def parse_utc_date_as_formatted(date, dest_tz="Asia/Seoul", format="%m월 %d일, %H:%M") -> str:
    date = parse_utc_date(date, dest_tz)
    return date.strftime("%m월 %d일, %H:%M".encode("unicode-escape").decode()).encode().decode("unicode-escape")


def parse_to_utc(date: Union[datetime, str], source_tz="Asia/Seoul") -> datetime:
    return (
        parse(date).replace(tzinfo=tz.gettz(source_tz)).astimezone(tz.gettz("UTC"))
        if isinstance(date, str)
        else date.replace(tzinfo=tz.gettz(source_tz)).astimezone(tz.gettz("UTC"))
    )


def reverse_geocoding(row):
    lat = row["lat"]
    lng = row["lng"]
    url = "https://naveropenapi.apigw.ntruss.com/map-reversegeocode/v2/gc"
    headers = {"X-NCP-APIGW-API-KEY-ID": navermaps_client["id"], "X-NCP-APIGW-API-KEY": navermaps_client["secret"]}
    params = {
        "coords": f"{lng},{lat}",
        "sourcecrs": "epsg:4326",  # GPS는 EPSG:4326 = WSG 84
        "orders": "admcode",  # 행정동
        "output": "json",
    }

    r = requests.get(url, headers=headers, params=params)
    results = r.json().get("results", [])
    if results is not None or len(results) > 0:
        result = results[0]
        address = reduce(make_address, list(result["region"].values()), "")
        return address.lstrip()

    return "주소를 가져오지 못함"


def make_address(acc: str, cur: Any):
    name = cur.get("name", "")
    if name not in ["kr", "", " "]:
        return f"{acc} {name}"
    return acc


def get_devices_id_by_user_id(user_id: int):
    device_ids = []
    for device in traccar_api.fetch_devices():
        if user_id == device["attributes"].get("userId"):
            device_ids.append(device["id"])
    return device_ids
