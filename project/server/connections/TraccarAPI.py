from typing import Optional, List, Any, TypedDict
from .TraccarConnection import TraccarConnection


class TcDevice(TypedDict):
    attributes: Any
    category: Optional[Any]
    contact: Optional[Any]
    disabled: bool
    geofenceIds: List[Any]
    groupId: int
    id: int
    lastUpdate: str
    model: Optional[Any]
    name: str
    phone: str
    positionId: int
    status: str
    uniqueId: str


class TraccarAPI:
    traccar: Optional[TraccarConnection]

    def __init__(self, traccar: Optional[TraccarConnection] = None):
        self.traccar = traccar

    def fetch_devices(self, **kwargs) -> List[TcDevice]:
        traccar = self.traccar
        params = {"userId": traccar.current_userId}

        if "uids" in kwargs.keys():
            uids = kwargs["uids"]
            if uids is not None and len(uids) != 0:
                params["uniqueId"] = uids
        try:
            res = traccar.GET("devices", params)
            return res
        except Exception as e:
            print("No such devices", e)
            return []

    def generate_uniqueId(self, phone):
        search = f"{phone[-4:]}%"
        devices = self.fetch_devices()
        check_ids = list(map(lambda x: x["uniqueId"], devices))
        print("ID DUP", search, len(check_ids))
        if len(check_ids) == 0:
            return phone[-4:]
        print(f"{phone[-4:]}{len(check_ids)}")
        return f"{phone[-4:]}{len(check_ids)}"

    def GET(self, *args, **kwargs):
        return self.traccar.GET(*args, **kwargs)

    def POST(self, *args, **kwargs):
        return self.traccar.POST(*args, **kwargs)

    def PUT(self, *args, **kwargs):
        return self.traccar.PUT(*args, **kwargs)
