import json
from typing import Dict, Any, Optional, Union, TextIO, BinaryIO, List, Tuple
import requests
from datetime import datetime
from urllib.parse import urljoin

RequestParam = Union[dict, List[Tuple[Any, Any]], bytes]
RequestBody = Union[dict, List[Tuple[Any, Any]], bytes, TextIO, BinaryIO]


class TraccarConnection:
    user: Dict[str, str] = {"email": "admin", "password": "admin"}
    server: str = "http://localhost"
    port: int = 8082
    base_url: str = ""
    session: requests.Session
    token: Optional[str]

    def __init__(self, conn_info={}):
        self.user = conn_info.get("user", {"email": "admin", "password": "admin"})
        self.server = conn_info.get("server", "http://localhost")
        self.port = conn_info.get("port", 8082)
        self.base_url = conn_info.get("base_url", "")

        self.url = urljoin(f"{self.server}:{self.port}", self.base_url)
        self.api = urljoin(self.url + "/", "api")
        print("[TRACCAR] API Endpoint", self.api)
        self.session = requests.session()
        self.token = None
        self.current_userId = None

    def __str__(self):
        return self.url

    def __repr__(self):
        return "Traccar Connection with " + self.url

    def get_session(self) -> Optional[Any]:
        url = self.api + "/session"
        params = {"token": self.token}
        try:
            res = self.session.get(url, params=params)
            if res.status_code == 200:
                print("[Traccar] CONNECTION ESTABLISED")
                return res.json()
            else:
                print("[Traccar] TOKEN LOGIN FAILED")
                return None
        except requests.exceptions.ConnectionError as e:
            print(f"[{datetime.now()}] Get session failed with:\n{e}")

    def delete_session(self):
        url = self.api + "/session"
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        try:
            res = self.session.delete(url, headers=headers)
            if res.status_code == 204:
                print("Connection Closed")
                print(res)
                return True
            else:
                print("Delete?")
                print(res)
        except requests.exceptions.ConnectionError as e:
            print(f"[{datetime.now()}] Delete session failed with:\n{e}")

    def connect(self) -> Optional[requests.Session]:
        url = self.api + "/session"
        if self.token:
            print("Trying token:", self.token)
            return self.get_session()
        else:
            data = self.user
            headers = {"Content-Type": "application/x-www-form-urlencoded"}
            try:
                res = self.session.post(url, data=data, headers=headers)
                if res.status_code == 200:
                    self.token = res.json().get("token")
                    self.current_userId = res.json().get("id")
                    return self.session
                else:
                    print("No traccar connection on", self.url)
                    print(res)
            except requests.exceptions.ConnectionError as e:
                print(f"[{datetime.now()}] Post login session failed with:\n{e}")
        return None

    def GET(self, endpoint: str, params: RequestParam = {}) -> Any:
        url = urljoin(self.api + "/", endpoint)
        self.connect()
        headers = {"Accept": "application/json"}
        try:
            res = self.session.get(url, headers=headers, params=params)
            print("GET-QUERY", res.url)
            if res.status_code == 200:
                return res.json()
            else:
                print("[Traccar] NO RESPONSE")
                return None
        except requests.exceptions.ConnectionError as e:
            print(f"[{datetime.now()}] Get session failed with:\n{e}")

    def POST(self, endpoint: str, data: RequestBody = {}):
        url = urljoin(self.api + "/", endpoint)
        self.connect()
        headers = {"Content-Type": "application/json"}
        try:
            res = self.session.post(url, data=json.dumps(data), headers=headers)
            if res.status_code == 200:
                return "OK"
            else:
                print("POST", res.content)
                return "FAILED"
        except Exception as err:
            print("Cannot PUT, No connection", err)
            raise

    def PUT(self, endpoint: str, data: RequestBody = {}):
        url = urljoin(self.api + "/", endpoint)
        self.connect()
        headers = {"Content-Type": "application/json"}
        try:
            res = self.session.put(url, data=json.dumps(data), headers=headers)
            if res.status_code == 200:
                return "OK"
            else:
                return "FAILED"
        except Exception as err:
            print("Cannot PUT, No connection", err)
            raise
