from typing import Any, Callable, Type
import json
import os

config_mode = os.getenv("FLASK_ENV", "development")


def read_properties(setting, filename="db.properties"):
    """Get secret setting or fail with ImproperlyConfigured"""
    with open(filename) as f:
        secrets = json.load(f)
        return secrets.get(setting)


def config_properties(env: str) -> Callable[[str], Any]:
    filename = "db"
    if env is not None:
        filename += "." + env
    filename += "." + "properties"
    return lambda name: read_properties(name, filename)


get_config = config_properties(config_mode)


class Config:
    SECRET_KEY = "gksdideogkrrysalt"
    DEBUG = False
    MONGO_URI = "{drivername}://{username}:{password}@{host}:{port}".format(**get_config("mongodb"))
    SQLALCHEMY_DATABASE_URI = "{drivername}://{username}:{password}@{host}:{port}/{database}".format(
        **get_config("postgres")
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ENGINE_OPTIONS = {"pool_pre_ping": True}
    NAVER_MAPS_CLIENT = get_config("navermapsClient")
    _DBLAB_GLOBAL_URL_PREFIX = get_config("urlPrefix")


class DevelopmentConfig(Config):
    DEBUG = True
    SEND_FILE_MAX_AGE_DEFAULT = 0


class DevOpsConfig(DevelopmentConfig):
    pass


class TestingConfig(Config):
    DEBUG = True
    TESTING = True


class ProductionConfig(Config):
    DEBUG = False    


config_by_name: dict[str, Type[Config]] = dict(
    development=DevelopmentConfig, devops=DevOpsConfig, test=TestingConfig, production=ProductionConfig, default=Config
)
current_config = config_by_name.get(config_mode, config_by_name["default"])
key = Config.SECRET_KEY
