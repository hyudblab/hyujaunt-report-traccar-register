from flask import Blueprint, request, jsonify
from flask_login import login_required, current_user
import pandas as pd
from . import mongo
from .helpers import reverse_geocoding, get_devices_id_by_user_id
from pprint import pprint


report = Blueprint("report", __name__, static_folder="static/report", static_url_path="/report")


@report.route("/app/<path:path>")
@login_required
def main(path):
    print(path)
    return "Proper values"


@report.route("/")
@login_required
def index():
    return report.send_static_file("dist/index.html")


@report.route("/stops")
@login_required
def stops():
    device_ids = get_devices_id_by_user_id(current_user.id)
    print("Search Stops for:", device_ids)
    df = pd.DataFrame()
    for device_id in device_ids:
        stops_df = load_stop_dataframe(device_id)
        if not stops_df.empty:
            stops_df["address"] = stops_df.apply(reverse_geocoding, axis=1)
            stops_df["answer"] = stops_df.apply(lambda row: get_answer(row.uid, row.name), axis=1)
            df = df.append(stops_df)
    if not df.empty:
        print("보내기", df.head())
        return df.to_json(orient="records", index=True)
    else:
        return jsonify([])


@report.route("/questionnaire", methods=["GET", "POST"])
@login_required
def questionnaire():
    if request.method == "GET":
        questions = list(mongo.cx["traccar"]["survey"].find({}, {"_id": 0}))
        questions = sorted(questions, key=lambda item: item["order"])
        return jsonify(questions)
    else:
        data = request.get_json()
        answers = mongo.cx["traccar"]["answer"]
        deviceid = data["deviceid"]
        stopId = data["stopId"]
        print("UPDATE answers")
        pprint(data)
        answers.update_one({"deviceid": deviceid, "stopId": stopId}, {"$set": data}, upsert=True)
        return "OK"


@report.route("/answers")
@login_required
def answers():
    device_ids = get_devices_id_by_user_id(current_user.id)
    answers = list(mongo.cx["traccar"]["answer"].find({"deviceid": {"$in": device_ids}}, {"_id": 0}))
    return jsonify(answers)


@report.route("/sample")
def sample():
    devices = mongo.cx["traccar"]["device"]
    device = devices.find_one({"deviceid": 3})
    stops = TrajDataFrame(device["stops"])
    return stops.to_json(orient="records", index=True)


@report.route("/update", methods=["PUT"])
def update():
    data = request.get_json()
    update_mongo(data)
    return "OK"


@report.route("/update/test")
def fnTest():
    return "OK"


def update_mongo(data):
    df = TrajDataFrame(data)
    deviceid = df.iloc[0]["uid"].item()
    devices = mongo.cx["traccar"]["device"]
    device = devices.find_one({"deviceid": deviceid})
    if device:
        cdf = TrajDataFrame(device["stops"])
        cdf = cdf.append(df, ignore_index=True)
        devices.update_one({"deviceid": deviceid}, {"$set": {"stops": cdf.to_dict("list")}})
    else:
        devices.insert_one({"deviceid": deviceid, "stops": df.to_dict("list")})
    return "OK"


def TrajDataFrame(data):
    df = pd.DataFrame(data)
    df["datetime"] = pd.to_datetime(df["datetime"], unit="ms")
    df["leaving_datetime"] = pd.to_datetime(df["leaving_datetime"], unit="ms")
    return df


def load_stop_dataframe(deviceid):
    devices = mongo.cx["traccar"]["device"]
    device = devices.find_one({"deviceid": deviceid})
    if not device:
        print(f"No Device {deviceid}")
        return pd.DataFrame()

    stops = device.get("stops")
    if not stops or len(stops) == 0:
        print(f"Device {deviceid} has no stops yet")
        return pd.DataFrame()
    return TrajDataFrame(stops)


def get_answer(deviceid, stop_id):
    print(f"Answer {stop_id} of Device {deviceid}")
    answer = mongo.cx["traccar"]["answer"].find_one({"deviceid": deviceid, "stopId": stop_id}, {"_id": 0})
    pprint(answer)
    return answer
