var gulp = require('gulp');
var once = require('async-once')

// script to deploy react build into flask blueprint directory
// it replaces values and folder names

// README
// how to use this:
// in the react app, run npm run build
// copy the resulting /build folder in /myblueprint
// create a template of a template in /templates/base as index.default.html

// install gulp, gulp-replace, gulp-rename
// run gulp from /myblueprint


// replace routes inside css files

// move other files

gulp.task('static', () => 
  gulp.src('./build/static/**/*')
    .pipe(gulp.dest('../static'))
);

gulp.task('manifest', () =>
  gulp.src('./build/manifest.json')
    .pipe(gulp.dest('../'))
)

gulp.task('default', gulp.series('static', 'manifest'))
