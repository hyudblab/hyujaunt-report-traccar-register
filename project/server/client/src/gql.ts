import { gql } from 'apollo-boost';

export const GET_ANSWER = gql`
query answer($uniqueid: String!, $pos_id: Int!, $qid: Int!) {
  answer(uniqueid: $uniqueid, pos_id: $pos_id, qid: $qid){    
    choices{
      index
      prompt
      mappedValue
    }
  }  
}
`

export const ADD_ANSWER = gql`
mutation addAnswer($answer: AnswerInput!) {
  addAnswer(answer: $answer){
    uniqueid
    pos_id
    qid
    choices {
      mappedValue
      prompt
    }
  }
}
`

export const UPDATE_ANSWERED_STOP = gql`
mutation updateAnsweredStop($stopInput: StopInput!) {
  updateAnsweredStop(stop: $stopInput){
    uniqueid    
    recentStops {
      id
      completed
    }
  }
}
`

export const GET_NOTIFICATION = gql`
  query notification{
    notification @client
    {
      index
      message
      variant
    }
  }
`

export const UPDATE_NOTIFICATION = gql`
  mutation updateNoti($index: Int!, $message: String!, $variant: String){
    updateNoti(index: $index, message: $message, variant: $variant) @client {
      index
      message
      variant
    }
  }
`