//import 'core-js/es/map'
//import 'core-js/es/set'
//import 'core-js/es/object/assign'
//import 'raf/polyfill'
//import 'isomorphic-unfetch'

import React from 'react';
import ReactDOM from 'react-dom';
import Main from './pages/main'
import * as serviceWorker from './serviceWorker';

import ApolloClient, { InMemoryCache } from 'apollo-boost'
import { ApolloProvider } from '@apollo/react-hooks';

import { BrowserRouter } from 'react-router-dom'
import { resolvers, typeDefs } from './resolvers';

const DEV_MODE = process.env.REACT_APP_DEV_MODE
let URI = 'http://localhost:4000/graphql';

console.log("DEV_MODE", DEV_MODE)
switch (DEV_MODE) {
  case 'dev':
    URI = 'http://localhost:4000/graphql'
    break
  case 'dbapp':
    URI = 'https://dbapp.hanyang.ac.kr/graphql'
    break
  case 'aws':
    URI = 'https://dbsrv.hanyang.ac.kr/graphql'
    break
  case 'devops':
    URI = 'http://192.168.1.140/graphql'
    break
}

console.log("CONNECTING...", URI)

const cache = new InMemoryCache()
const client = new ApolloClient({
  cache,
  uri: URI,
  resolvers: resolvers,
  typeDefs: typeDefs
})

cache.writeData({
  data: {
    notification: {
      index: 0,
      message: "알림 없음",
      variant: "success",
      __typename: 'Notification'
    }
  }
})


ReactDOM.render(
  <BrowserRouter>
    <ApolloProvider client={client}>
      <Main />
    </ApolloProvider>
  </BrowserRouter>,
  document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
