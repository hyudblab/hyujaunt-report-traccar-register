import { RouteComponentProps } from 'react-router-dom'

declare module 'jaunt' {
  namespace Jaunt {
    // Device
    export interface Device {
      uniqueid: string
      recentStops: Stop[]
    }

    export interface Stop {
      cluster: number
      lat: number
      lng: number
      id: number
      leaving_datetime: string
      completed?: boolean
    }

    export interface DeviceData {
      device: Device
    }

    export interface DeviceRouterProps {
      uniqueid: string
    }

    export interface DeviceStopRouterProps extends DeviceRouterProps {
      pos_id: string
    }

    export interface DeviceVars extends RouteComponentProps<DeviceRouterProps> {

    }
    export interface DeviceStopVars extends RouteComponentProps<DeviceStopRouterProps> {

    }

    // Answer
    export interface Answer extends Question {
      uniqueid: string
      pos_id: number
    }

    export interface AnswerData {
      answer: Answer
    }

    export interface Choice {
      index: number
      prompt: string
      mappedValue: any
    }

    export interface Question {
      qid: number
      type: string
      text: string
      isMultiple: boolean
      shuffle: boolean
      choices: Choice[]
    }

    export interface FetchQuestionData {
      fetchQuestions: Question[]
    }
  }
  export = Jaunt
}
