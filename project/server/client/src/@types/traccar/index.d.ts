declare module 'traccar' {
  namespace Traccar {
    export interface TcDeviceAttributes {
      enrolledBy: string
    }

    export interface TcDevice {
      attributes: TcDeviceAttributes,
      category?: any,
      contact?: any,
      disabled: boolean,
      geofenceIds: Array<number>
      groupId: number
      id: number
      lastUpdate: string
      model?: any
      name: string
      phone?: string
      positionId: number
      uniqueId: string
      status: string
    }
  }
  export = Traccar
}
