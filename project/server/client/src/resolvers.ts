import gql from 'graphql-tag'
import { GET_NOTIFICATION } from './gql'

// Client: Notification
export interface Notification {
  message: string
  variant?: string
}

// Client: Graphql Resolver
export const typeDefs = gql`
  extend type Notification {
    index: Int
    message: String
    variant: String
  }

  extend type Query {
    notification: Notification
  }  

  extend type Mutation {
    updateNoti(notification: Notification): Notification
  }
`
export const resolvers: Resolvers = {
  Query: {
    notification: (_root, _, { cache }) => {
      const { message, variant } = cache.readQuery({ query: GET_NOTIFICATION })
      console.log("여기..?")
      return { message: message, variant: variant, __typename: "Notification" }
    }
  },
  Mutation: {
    updateNoti: (_, variables, { cache }) => {
      const { index, message, variant } = variables
      const data = {
        index: index,
        message: message,
        variant: variant,
        __typename: "Notification"
      }
      cache.writeData({
        data: {
          notification: data
        }
      })
      return data
    }
  }
}


interface Resolvers {
  [key: string]: {
    [field: string]: (
      rootValue?: any,
      args?: any,
      context?: any,
      info?: any,
    ) => any;
  };
}

type FragmentMatcher = (
  rootValue: any,
  typeCondition: string,
  context: any,
) => boolean;