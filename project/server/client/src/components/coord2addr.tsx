import React, { useState, useEffect, Fragment } from 'react'
import { coordToAddr } from '../daum-map-utils';


interface StopInfo {
  lat: number
  lng: number
}

const Coord2Addr: React.FC<StopInfo> = ({ lat, lng }) => {
  const [data, setData] = useState("알 수 없는 주소")

  useEffect(() => {
    const fetchData = async () => {
      const result = await coordToAddr(lat, lng)
      setData(result)
    }
    fetchData()
  }, [])
  return (
    <Fragment>
      {data}
    </Fragment>
  )
}

export default Coord2Addr