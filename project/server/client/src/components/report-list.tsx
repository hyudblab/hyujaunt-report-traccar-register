import React, { Fragment } from 'react'
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItem, ListItemText, Typography } from '@material-ui/core';
import { DeviceStopRouterProps, FetchQuestionData } from 'jaunt'
import SimpleChoice from './simple-choice';
import MultipleChoice from './multiple-choice';

const useStyles = makeStyles(theme => ({
  lists: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,    
  },
}));

const FETCH_QUESTIONS = gql`
query fetchQuestions {
  fetchQuestions{
    qid
    type
    text
    isMultiple
    shuffle
    choices {      
      index
      prompt
      mappedValue
    }
  }
}
`
const ReportList: React.FC<DeviceStopRouterProps> = ({ uniqueid, pos_id }) => {
  const classes = useStyles()
  const { loading, error, data } = useQuery<FetchQuestionData, {}>(
    FETCH_QUESTIONS,
    {
      fetchPolicy: "network-only"
    }
  )
  if (loading) return <p>Loading...</p>
  if (error) return <p>Error: {error.message}</p>

  return (
    <div className={classes.lists}>
      <List>
        <ListItem>
          <ListItemText>
            <Typography>문항을 선택하면 답변이 자동으로 저장됩니다.</Typography>
          </ListItemText>
        </ListItem>
        {data && data.fetchQuestions.map((question, listIndex) => {
          switch (question.isMultiple) {
            case false:
              return <ListItem key={question.qid}><ListItemText>
                <SimpleChoice
                  uniqueid={uniqueid}
                  pos_id={Number(pos_id)}
                  listIndex={listIndex + 1}
                  {...question} />
              </ListItemText></ListItem>

            case true:
              return <ListItem key={question.qid}><ListItemText>
                <MultipleChoice
                  uniqueid={uniqueid}
                  pos_id={Number(pos_id)}
                  listIndex={listIndex + 1}
                  {...question} />
              </ListItemText></ListItem>

            default:
              return <ListItem><ListItemText>
                <Fragment>
                  <Typography>알 수 없는 질문 형식입니다. {question.qid}</Typography>
                  <Typography>{question.text}</Typography>
                </Fragment>
              </ListItemText></ListItem>
          }
        })}
      </List>
    </div>
  )
}

export default ReportList