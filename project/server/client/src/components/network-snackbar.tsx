import React, { SyntheticEvent } from 'react';
import clsx from 'clsx'
import { makeStyles, Theme } from '@material-ui/core/styles';
import { green, amber } from '@material-ui/core/colors';
import { Snackbar, SnackbarContent } from '@material-ui/core'
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import WarningIcon from '@material-ui/icons/Warning';

const variantIcon = {
  success: CheckCircleIcon,  
  warning: WarningIcon
};

const useStyles = makeStyles((theme: Theme) => ({
  success: {
    backgroundColor: green[600],
  },  
  warning: {
    backgroundColor: amber[700],
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
  margin: {
    margin: theme.spacing(1),
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
}))

interface IProps {
  message: string
  variant: keyof typeof variantIcon
}

const NetworkSnackbar: React.FC<IProps> = ({ variant, message }) => {
  const classes = useStyles();
  const Icon = variantIcon[variant];
  const [state, setState] = React.useState({
    open: false,
    message: message
  });

  if (message && message.length !== 0 && state.message !== message) {
    setState({ open: true, message: message });
  }

  function handleClose(event?: SyntheticEvent, reason?: string) {
    if (reason === 'clickaway') {
      return;
    }
    setState({ open: false, message: message });
  }

  return (
    <div>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={state.open}
        autoHideDuration={3000}
        onClose={handleClose}
      >
        <SnackbarContent
          className={clsx(classes[variant], variant)}
          aria-describedby="client-snackbar"          
          message={
            <span id="client-snackbar" className={classes.message}>
              <Icon className={clsx(classes.icon, classes.iconVariant)} />
              {message}
            </span>
          } />
      </Snackbar>
    </div>
  );
}

export default NetworkSnackbar