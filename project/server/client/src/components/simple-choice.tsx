import React, { useState, Fragment } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { FormControl, FormLabel, RadioGroup, FormControlLabel, Radio } from '@material-ui/core';
import { GET_ANSWER, ADD_ANSWER, UPDATE_NOTIFICATION } from '../gql'
import { Answer, AnswerData, Choice } from 'jaunt'
import { useQuery, useMutation } from '@apollo/react-hooks';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
    },
  }),
);

const ChoiceItem: React.FC<Choice> = (choice) => {
  return (
    <FormControlLabel
      value={choice.mappedValue}
      control={<Radio />}
      label={choice.prompt} />
  )
}

interface QuestionItemProp extends Answer {
  listIndex: number
}

const SimpleChoice: React.FC<QuestionItemProp> = (props) => {
  const classes = useStyles()
  const { uniqueid, qid, pos_id, choices } = props
  const [value, setValue] = useState<string>("-1");
  const { loading, error } = useQuery<AnswerData, {}>(
    GET_ANSWER,
    {
      variables: { uniqueid, qid, pos_id },
      fetchPolicy: "network-only",
      onCompleted: data => {
        data.answer && setValue(data.answer.choices[0].mappedValue)
      }
    }
  )

  const [addAnswer,] = useMutation(ADD_ANSWER)
  const [updateNoti,] = useMutation(UPDATE_NOTIFICATION)
  if (loading) return <p>Loading...</p>
  if (error) return <p>Error: {error.message}</p>

  function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    const selected = (event.target as HTMLInputElement).value
    const choice = choices.find(element => (element.mappedValue === selected ? element : undefined))
    console.log(choice)
    if (choice) {
      addAnswer({
        variables: {
          answer: {
            uniqueid: uniqueid,
            qid: qid,
            pos_id: pos_id,
            choices: [{
              index: choice.index,
              mappedValue: choice.mappedValue,
              prompt: choice.prompt
            }]
          }
        }
      }).then(() => {
        updateNoti({
          variables: {
            index: props.listIndex,
            message: `서버에 저장됨: "Q${props.listIndex}: ${choice.prompt}"`,
            variant: "success"
          }
        })
      })
      setValue(selected);
    }    
  }

  return (
    <Fragment>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">Q{props.listIndex}. {props.text}</FormLabel>
        <RadioGroup aria-label="question" name={"question" + qid} value={value} onChange={handleChange}>
          {choices.map((choice, index) => (<ChoiceItem key={index} {...choice} />))}
        </RadioGroup>
      </FormControl>
    </Fragment>
  )
}

export default SimpleChoice