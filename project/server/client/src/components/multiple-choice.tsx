import React, { useState, Fragment } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import {
  FormLabel,
  FormControl,
  FormGroup,
  FormControlLabel,
  Checkbox
}
  from '@material-ui/core';
import { GET_ANSWER, ADD_ANSWER, UPDATE_NOTIFICATION } from '../gql'
import { Answer, AnswerData, Choice } from 'jaunt'
import { useQuery, useMutation } from '@apollo/react-hooks';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
    },
  }),
);

interface QuestionItemProp extends Answer {
  listIndex: number
}

const MultipleChoice: React.FC<QuestionItemProp> = (props) => {
  const classes = useStyles()
  const { uniqueid, pos_id, qid, choices } = props
  const [state, setState] = useState<any[]>([])  
  const { loading, error, } = useQuery<AnswerData, {}>(
    GET_ANSWER,
    {
      variables: { uniqueid, pos_id, qid },
      fetchPolicy: "network-only",
      onCompleted: data => {
        if (data.answer) {
          setState(data.answer.choices.map(choice => choice.mappedValue))
        }
      }
    }
  )
  const [addAnswer,] = useMutation(ADD_ANSWER)
  const [updateNoti,] = useMutation(UPDATE_NOTIFICATION)
  if (loading) return <p>Loading...</p>
  if (error) return <p>Error: {error.message}</p>

  const handleChange = (name: string) => (event: React.ChangeEvent<HTMLInputElement>) => {    
    let newMsg = {
      message: `서버에 저장됨: "Q${props.listIndex}: ${name}" `,
      variant: "success" as "success" | "warning"
    }
    let newState = new Array<Choice>(0)
    // 체크할 때
    if (event.target.checked) {
      newState = [...state, name]
      newMsg.message += '추가'
    }
    // 체크 해제할 때    
    else {
      newState = state.filter(value => value !== name)
      newMsg.message += '삭제'
      newMsg.variant = "warning"
    }
    const newChoices = choices.filter(choice => newState.includes(choice.mappedValue))
    addAnswer({
      variables: {
        answer: {
          uniqueid: uniqueid,
          pos_id: pos_id,
          qid: qid,
          choices: newChoices.map(choice => ({
            index: choice.index,
            mappedValue: choice.mappedValue,
            prompt: choice.prompt
          }))
        }
      }
    }).then(() => {      
      updateNoti({
        variables: {
          index: props.listIndex,
          message: newMsg.message,
          variant: newMsg.variant
        }
      })
      setState(newState)
    })

  };

  return (
    <Fragment>
      <FormControl component="fieldset" className={classes.formControl}>
        <FormLabel component="legend">Q{props.listIndex}. {props.text}</FormLabel>
        <FormGroup>
          {choices.map((choice, index) => (
            <FormControlLabel
              key={index}
              control={
                <Checkbox
                  checked={state.includes(choice.mappedValue)}
                  onChange={handleChange(choice.mappedValue)}
                  value={choice.mappedValue}
                />}
              label={choice.prompt}
            />
          ))}
        </FormGroup>
      </FormControl>      
    </Fragment>
  )
}

export default MultipleChoice