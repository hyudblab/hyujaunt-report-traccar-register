import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container } from '@material-ui/core';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import Header from '../components/header'
import StopList from './stop-list'
import { DeviceData, DeviceVars } from 'jaunt'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
}));


const GET_DEVICE = gql`
query device($uniqueid: String!) {  
  device(uniqueid: $uniqueid){
    uniqueid
    recentStops {
      id
      lat
      lng
      cluster
      leaving_datetime
      completed
    }
  }  
}
`

const DeviceStops: React.FC<DeviceVars> = ({ match }) => {
  const { uniqueid } = match.params
  const classes = useStyles()
  const { loading, error, data } = useQuery<DeviceData, {}>(
    GET_DEVICE,
    {
      variables: { uniqueid },
      fetchPolicy: "network-only"
    }
  )
  if (loading) return <p>Loading...</p>
  if (error) return <p>Error: {error.message}</p>

  console.log('장소들', data)  

  return (
    <Container maxWidth="sm" className={classes.root}>
      <Header title={uniqueid + "의 최근 방문 장소"} />
      {data && data.device ? <StopList {...data.device} /> : <p>최근 방문 장소가 없습니다</p>}
    </Container>
  )
}

export default DeviceStops;
