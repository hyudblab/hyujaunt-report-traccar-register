import React from 'react';
import { Route, Switch } from "react-router-dom";
import DeviceStops from './device-stops'
import Report from './report'
import NoMatch from './no-match'
import { BASE_URI } from '../route-config'

const Main: React.FC = () => {
  return (
    <Switch>      
      <Route exact path={BASE_URI + "/device/:uniqueid/:pos_id"} component={Report} />
      <Route path={BASE_URI + "/device/:uniqueid"} component={DeviceStops} />
      <Route exact path={BASE_URI + "/debug/device/:uniqueid/:pos_id"} component={Report} />
      <Route path={BASE_URI + "/debug/device/:uniqueid"} component={DeviceStops} />
      <Route component={NoMatch} />
    </Switch>
  )
}

export default Main;
