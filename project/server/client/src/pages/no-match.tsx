import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Divider, Button, Typography } from '@material-ui/core';
import Header from '../components/header'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  refreshButton: {
    backgroundColor: '#1111CC',
    fontWeight: 'bold',
    fontSize: '20px',
    color: 'white'
  }
}));

const NoMatch: React.FC = () => {
  const classes = useStyles()

  const handleRefresh = () => {
    window.location.reload(false)
  }

  return (
    <Container maxWidth="sm" className={classes.root}>
      <Header title={"ERROR! 오류발생"} />
      <Typography align="center" variant="h4">사용자 정보 또는 방문 장소 정보가 없습니다.</Typography>
      <Divider variant='middle'></Divider>
      <Typography align="center" variant="h6">Traccar를 재시작 한 뒤 접속해주세요.</Typography>
      <Typography align="center" variant="h6">또는
      <span onClick={handleRefresh}>
          <Button className={classes.refreshButton} variant="contained" >새로고침</Button>
        </span>
        해보세요.</Typography>
      <Typography align="center" variant="h6">문제가 계속되면 관리자에게 문의해주세요.</Typography>
      <Divider variant='middle'></Divider>
    </Container>
  )
}

export default NoMatch;