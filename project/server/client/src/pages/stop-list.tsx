import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Device } from 'jaunt'
import { Typography, Divider } from '@material-ui/core';
import { BASE_URI } from '../route-config';
import Coord2Addr from '../components/coord2addr';

const useStyles = makeStyles(theme => ({
  lists: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  completed: {
    backgroundColor: 'lightgray'
  }
}));

const StopList: React.FC<Device> = ({ uniqueid, recentStops }) => {
  const classes = useStyles();
  const options = {
    timeZone: 'UTC',
    year: 'numeric', month: 'numeric', day: 'numeric',
    weekday: "short", hour: '2-digit', minute: '2-digit'
  }

  return (
    <div className={classes.lists}>
      <List component="nav" aria-label="main folders">
        {recentStops.map(stop => {
          if (stop.completed) {
            return <Fragment key={stop.id}>
              <ListItem className={classes.completed}>
                <ListItemText>
                  <Typography>완료된 설문입니다.</Typography>                  
                </ListItemText>
              </ListItem>
              <Divider component="li" />
            </Fragment>
          }
          else {
            return <Fragment key={stop.id}>
              <ListItem button component={Link} to={BASE_URI + '/device/' + uniqueid + '/' + stop.id}>
                <ListItemText>
                  <Typography>
                    {stop.lat}, {stop.lng}
                  </Typography>
                  <Typography>주소: <Coord2Addr {...stop}></Coord2Addr></Typography>
                  <Typography>
                    방문시각: {new Intl.DateTimeFormat('ko-KR', options).format(new Date(Number(stop.leaving_datetime)))}
                  </Typography>
                </ListItemText>
              </ListItem>
              <Divider component="li" />
            </Fragment>
          }
        })
        }
      </List>
    </div>
  )
}

export default StopList