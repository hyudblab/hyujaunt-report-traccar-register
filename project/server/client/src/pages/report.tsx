import React, { useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Container, Divider, List, ListItem, ListItemText, Typography } from '@material-ui/core';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import Header from '../components/header'
import NetworkSnackbar from '../components/network-snackbar'
import { DeviceData, DeviceStopVars } from 'jaunt'
import { Map, Marker } from 'react-kakao-maps'
import { Redirect } from 'react-router';
import Coord2Addr from '../components/coord2addr';
import ReportList from '../components/report-list'
import { GET_NOTIFICATION, UPDATE_ANSWERED_STOP } from '../gql';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  map: {
    height: '24vh',
  },
  infoList: {
    backgroundColor: "#c6e6f5"
  },
  infoItems: {
    padding: '0',
    textAlign: "center"
  },
  button: {
    margin: theme.spacing(1),
  },
}));


const GET_DEVICE_STOPS = gql`
query device($uniqueid: String!) {  
  device(uniqueid: $uniqueid){    
    recentStops {
      lat
      lng      
      leaving_datetime
      id
    }
  }  
}
`

const Report: React.FC<DeviceStopVars> = ({ history, match }) => {
  const { uniqueid, pos_id } = match.params
  const classes = useStyles()
  const { loading, error, data } = useQuery<DeviceData, {}>(
    GET_DEVICE_STOPS,
    {
      variables: { uniqueid },
      fetchPolicy: "network-only"
    }
  )
  const [updateAnsweredStop,] = useMutation(UPDATE_ANSWERED_STOP)
  const notiResult = useQuery(GET_NOTIFICATION)
  const snackbarMsg = useMemo(() => notiResult.data.notification, [notiResult.data.notification])
  if (loading) return <p>Loading...</p>
  if (error) return <p>Error: {error.message}</p>

  console.log(snackbarMsg)

  const stopInfo = data && data.device.recentStops.find(element => element.id === Number(pos_id))

  function handleCompleteAnswer() {
    console.log(typeof(uniqueid), typeof(parseInt(pos_id)), true)
    console.log(uniqueid, parseInt(pos_id), true)
    updateAnsweredStop({
      variables: {
        "stopInput": {
          "uniqueid": uniqueid,    
          "pos_id": parseInt(pos_id),
          "completed": true
        }
      }
    }).then(() => {      
      history.goBack()
    })
  }

  if (stopInfo) {
    let leavingDatetime = new Date(Number(stopInfo.leaving_datetime))
    const options = {
      timeZone: 'UTC',
      year: 'numeric', month: 'numeric', day: 'numeric',
      weekday: "short", hour: '2-digit', minute: '2-digit'
    }

    console.log(stopInfo)

    return (
      <Container maxWidth="sm" className={classes.root}>
        <Header title="설문조사">

        </Header>
        <div className={classes.map}>
          <Map options={{
            center: new daum.maps.LatLng(stopInfo.lat, stopInfo.lng),
            level: 3
          }}>
            <Marker options={{
              position: new daum.maps.LatLng(stopInfo.lat, stopInfo.lng)
            }} />
          </Map>
        </div>
        <List dense={true} className={classes.infoList}>
          <ListItem className={classes.infoItems}>
            <ListItemText>
              <Typography>
                좌표정보: {stopInfo.lat}, {stopInfo.lng}
              </Typography>
            </ListItemText>
          </ListItem>
          <ListItem className={classes.infoItems}>
            <ListItemText>
              <Typography>
                <Coord2Addr {...stopInfo}></Coord2Addr>
              </Typography>
            </ListItemText>
          </ListItem>
          <ListItem className={classes.infoItems}>
            <ListItemText>
              <Typography>
                방문시각: {new Intl.DateTimeFormat('ko-KR', options).format(leavingDatetime)}
              </Typography>
            </ListItemText>
          </ListItem>
        </List>
        <Divider />
        <ReportList uniqueid={uniqueid} pos_id={pos_id}></ReportList>
        <Button onClick={handleCompleteAnswer} variant="contained" color="primary" fullWidth aria-label="full width" className={classes.button}>
          설문 종료하기
        </Button>
        <NetworkSnackbar {...snackbarMsg} />
      </Container>
    )
  }

  return (
    <Redirect to="/NotFound" />
  )
}

export default Report;
