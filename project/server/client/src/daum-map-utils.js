var daum = window.daum

export const coordToAddr = (lat, lng) => {
  return new Promise((resolve, reject) => {
    try {
      let geocoder = new daum.maps.services.Geocoder()
      geocoder.coord2Address(lng, lat, (result, status) => {
        if (status === daum.maps.services.Status.OK) resolve(result[0].address.address_name)
        else resolve("주소를 찾지 못함")
      })
    } catch (e) {
      reject(new Error("daum map hasn't loaded"))
    }
  })
}