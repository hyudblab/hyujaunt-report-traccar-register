from flask import Blueprint, render_template, request, redirect, url_for
from flask_login import login_required, current_user
import pandas as pd
from itertools import chain
from sqlalchemy import not_
from . import mongo
from .main import fetch_devices

dashboard = Blueprint('dashboard', __name__,
                      static_folder='server-static', static_url_path="/server-static")


@dashboard.route('/')
def index():
    devices = TcDevice.query.with_entities(TcDevice.uniqueid, TcDevice.lastupdate).filter(not_(
        TcDevice.name.like('@%'))).filter(not_(TcDevice.name.like('#%'))).filter(TcDevice.name != '01').filter(TcDevice.name != '02').all()
    #devices = [x[0] for x in devices]
    return render_template("dashboard.html", devices=devices)


@dashboard.route('/<uid>')
def detail(uid):
    # answers_by_uid
    name = fetch_devices(uids=[uid])[0].name
    answers = list_user_answers([uid])
    print(type(answers))
    return render_template("dashboard-detail.html", name=name, answers=answers)


def list_user_answers(devices):
    # mongo - stops
    stop_data = mongo.db.device.find({
        "uniqueid": {'$in': devices}
    })
    all_stops = []
    for user in list(stop_data):
        all_stops.append(user['recentStops'])
    # Flattening trick
    # stop_df = pd.DataFrame(sum(all_stops, [])) # Slower
    stop_df = pd.DataFrame(chain.from_iterable(all_stops))

    # mongo - answers
    try:
        answer_data = mongo.db['answer'].find({
            "uniqueid": {'$in': devices}
        })
        answer_df = pd.DataFrame(list(answer_data))
        answer_df['datetime'] = pd.to_datetime(
            answer_df['timestamp'].astype('Q'), unit='ms')
        answer_df.datetime = answer_df.datetime.dt.tz_localize(
            'UTC').dt.tz_convert('Asia/Seoul')
    except KeyError as e:
        return []

    # mongo - question
    question_data = mongo.db['question'].find()
    question_df = pd.DataFrame(list(question_data))

    # TODO: 디바이스 데이터프레임 붙이기
    return answers_by_uid(devices[0], stop_df, answer_df, question_df)


def answers_by_uid(uid, stop_df, answer_df, question_df):
    display_list = []
    stop_pos_ids = answer_df[(answer_df['uniqueid'] == uid)
                             ]['pos_id'].dropna().unique()
    print(f"유저: {uid}, {stop_pos_ids}")
    for pid in stop_pos_ids:
        print(f"위치: {pid}")
        for qid in range(5):
            print(
                f"질문번호: {qid + 1}, 질문: {question_df[question_df['qid'] == qid]['text'].iloc[0]}")
            # get last answers only: index -1
            display = {
                'uid': uid,
                'qid': qid,
                'pid': pid
            }
            try:
                display['qtext'] = question_df[question_df['qid']
                                               == qid]['text'].iloc[-1]
                display['datetime'] = stop_df[stop_df['id']
                                              == pid]['datetime'].iloc[-1]
                display['leaving_datetime'] = stop_df[stop_df['id']
                                                      == pid]['leaving_datetime'].iloc[-1]
            except Exception as e:
                print(e)
            try:
                display['answer_time'] = answer_df[(answer_df['pos_id'] == pid) & (
                    answer_df['qid'] == qid)]['datetime'].iloc[-1]
                display['choices'] = answer_df[(answer_df['pos_id'] == pid) & (
                    answer_df['qid'] == qid)]['choices'].iloc[-1]
            except Exception as e:
                print(e)
            display_list.append(display)
    return display_list
