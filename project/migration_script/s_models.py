# coding: utf-8
from sqlalchemy import Boolean, Column, DateTime, Float, ForeignKey, Index, Integer, LargeBinary, String, Table, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class User(Base):
    __tablename__ = 'User'

    id = Column(Integer, primary_key=True, server_default=text("nextval('volunteer_id_seq'::regclass)"))
    email = Column(String(100), unique=True)
    password = Column(String(100), nullable=False)
    name = Column(String(100), nullable=False)


t_databasechangelog = Table(
    'databasechangelog', metadata,
    Column('id', String(255), nullable=False),
    Column('author', String(255), nullable=False),
    Column('filename', String(255), nullable=False),
    Column('dateexecuted', DateTime, nullable=False),
    Column('orderexecuted', Integer, nullable=False),
    Column('exectype', String(10), nullable=False),
    Column('md5sum', String(35)),
    Column('description', String(255)),
    Column('comments', String(255)),
    Column('tag', String(255)),
    Column('liquibase', String(20)),
    Column('contexts', String(255)),
    Column('labels', String(255)),
    Column('deployment_id', String(10))
)


class Databasechangeloglock(Base):
    __tablename__ = 'databasechangeloglock'

    id = Column(Integer, primary_key=True)
    locked = Column(Boolean, nullable=False)
    lockgranted = Column(DateTime)
    lockedby = Column(String(255))


class Recruit(Base):
    __tablename__ = 'recruit'

    id = Column(Integer, primary_key=True, server_default=text("nextval('recruit_id_seq'::regclass)"))
    sid = Column(String(12), unique=True)
    email = Column(String(100), nullable=False)
    name = Column(String(100), nullable=False)
    phone = Column(String(14), nullable=False)
    sex = Column(String(1), nullable=False)


class TcAttribute(Base):
    __tablename__ = 'tc_attributes'

    id = Column(Integer, primary_key=True)
    description = Column(String(4000), nullable=False)
    type = Column(String(128), nullable=False)
    attribute = Column(String(128), nullable=False)
    expression = Column(String(4000), nullable=False)

    tc_devices = relationship('TcDevice', secondary='tc_device_attribute')
    tc_groups = relationship('TcGroup', secondary='tc_group_attribute')
    tc_users = relationship('TcUser', secondary='tc_user_attribute')


class TcCalendar(Base):
    __tablename__ = 'tc_calendars'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), nullable=False)
    data = Column(LargeBinary, nullable=False)
    attributes = Column(String(4000), nullable=False)

    tc_users = relationship('TcUser', secondary='tc_user_calendar')


class TcCommand(Base):
    __tablename__ = 'tc_commands'

    id = Column(Integer, primary_key=True)
    description = Column(String(4000), nullable=False)
    type = Column(String(128), nullable=False)
    textchannel = Column(Boolean, nullable=False, server_default=text("false"))
    attributes = Column(String(4000), nullable=False)

    tc_groups = relationship('TcGroup', secondary='tc_group_command')
    tc_users = relationship('TcUser', secondary='tc_user_command')
    tc_devices = relationship('TcDevice', secondary='tc_device_command')


class TcDriver(Base):
    __tablename__ = 'tc_drivers'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), nullable=False)
    uniqueid = Column(String(128), nullable=False, unique=True)
    attributes = Column(String(4000), nullable=False)

    tc_groups = relationship('TcGroup', secondary='tc_group_driver')
    tc_users = relationship('TcUser', secondary='tc_user_driver')


class TcGroup(Base):
    __tablename__ = 'tc_groups'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), nullable=False)
    groupid = Column(ForeignKey('tc_groups.id', ondelete='SET NULL', onupdate='RESTRICT'))
    attributes = Column(String(4000))

    parent = relationship('TcGroup', remote_side=[id])
    tc_maintenances = relationship('TcMaintenance', secondary='tc_group_maintenance')
    tc_notifications = relationship('TcNotification', secondary='tc_group_notification')
    tc_users = relationship('TcUser', secondary='tc_user_group')


class TcMaintenance(Base):
    __tablename__ = 'tc_maintenances'

    id = Column(Integer, primary_key=True)
    name = Column(String(4000), nullable=False)
    type = Column(String(128), nullable=False)
    start = Column(Float(53), nullable=False, server_default=text("0"))
    period = Column(Float(53), nullable=False, server_default=text("0"))
    attributes = Column(String(4000), nullable=False)

    tc_users = relationship('TcUser', secondary='tc_user_maintenance')


class TcServer(Base):
    __tablename__ = 'tc_servers'

    id = Column(Integer, primary_key=True)
    registration = Column(Boolean, nullable=False, server_default=text("true"))
    latitude = Column(Float(53), nullable=False, server_default=text("0"))
    longitude = Column(Float(53), nullable=False, server_default=text("0"))
    zoom = Column(Integer, nullable=False, server_default=text("0"))
    map = Column(String(128))
    bingkey = Column(String(128))
    mapurl = Column(String(512))
    readonly = Column(Boolean, nullable=False, server_default=text("false"))
    twelvehourformat = Column(Boolean, nullable=False, server_default=text("false"))
    attributes = Column(String(4000))
    forcesettings = Column(Boolean, nullable=False, server_default=text("false"))
    coordinateformat = Column(String(128))
    devicereadonly = Column(Boolean, server_default=text("false"))
    limitcommands = Column(Boolean, server_default=text("false"))
    poilayer = Column(String(512))


class TcStatistic(Base):
    __tablename__ = 'tc_statistics'

    id = Column(Integer, primary_key=True)
    capturetime = Column(DateTime, nullable=False)
    activeusers = Column(Integer, nullable=False, server_default=text("0"))
    activedevices = Column(Integer, nullable=False, server_default=text("0"))
    requests = Column(Integer, nullable=False, server_default=text("0"))
    messagesreceived = Column(Integer, nullable=False, server_default=text("0"))
    messagesstored = Column(Integer, nullable=False, server_default=text("0"))
    attributes = Column(String(4096), nullable=False)
    mailsent = Column(Integer, nullable=False, server_default=text("0"))
    smssent = Column(Integer, nullable=False, server_default=text("0"))
    geocoderrequests = Column(Integer, nullable=False, server_default=text("0"))
    geolocationrequests = Column(Integer, nullable=False, server_default=text("0"))
    protocols = Column(String(4096))


class TcUser(Base):
    __tablename__ = 'tc_users'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), nullable=False)
    email = Column(String(128), nullable=False, unique=True)
    hashedpassword = Column(String(128))
    salt = Column(String(128))
    readonly = Column(Boolean, nullable=False, server_default=text("false"))
    administrator = Column(Boolean)
    map = Column(String(128))
    latitude = Column(Float(53), nullable=False, server_default=text("0"))
    longitude = Column(Float(53), nullable=False, server_default=text("0"))
    zoom = Column(Integer, nullable=False, server_default=text("0"))
    twelvehourformat = Column(Boolean, nullable=False, server_default=text("false"))
    attributes = Column(String(4000))
    coordinateformat = Column(String(128))
    disabled = Column(Boolean, server_default=text("false"))
    expirationtime = Column(DateTime)
    devicelimit = Column(Integer, server_default=text("'-1'::integer"))
    token = Column(String(128))
    userlimit = Column(Integer, server_default=text("0"))
    devicereadonly = Column(Boolean, server_default=text("false"))
    phone = Column(String(128))
    limitcommands = Column(Boolean, server_default=text("false"))
    login = Column(String(128))
    poilayer = Column(String(512))

    parents = relationship(
        'TcUser',
        secondary='tc_user_user',
        primaryjoin='TcUser.id == tc_user_user.c.manageduserid',
        secondaryjoin='TcUser.id == tc_user_user.c.userid'
    )


class TcDevice(Base):
    __tablename__ = 'tc_devices'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), nullable=False)
    uniqueid = Column(String(128), nullable=False, unique=True)
    lastupdate = Column(DateTime)
    positionid = Column(Integer)
    groupid = Column(ForeignKey('tc_groups.id', ondelete='SET NULL'))
    attributes = Column(String(4000))
    phone = Column(String(128))
    model = Column(String(128))
    contact = Column(String(512))
    category = Column(String(128))
    disabled = Column(Boolean, server_default=text("false"))

    tc_group = relationship('TcGroup')
    tc_drivers = relationship('TcDriver', secondary='tc_device_driver')
    tc_notifications = relationship('TcNotification', secondary='tc_device_notification')
    tc_geofences = relationship('TcGeofence', secondary='tc_device_geofence')
    tc_maintenances = relationship('TcMaintenance', secondary='tc_device_maintenance')
    tc_users = relationship('TcUser', secondary='tc_user_device')


class TcGeofence(Base):
    __tablename__ = 'tc_geofences'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), nullable=False)
    description = Column(String(128))
    area = Column(String(4096), nullable=False)
    attributes = Column(String(4000))
    calendarid = Column(ForeignKey('tc_calendars.id', ondelete='SET NULL'))

    tc_calendar = relationship('TcCalendar')
    tc_groups = relationship('TcGroup', secondary='tc_group_geofence')
    tc_users = relationship('TcUser', secondary='tc_user_geofence')


t_tc_group_attribute = Table(
    'tc_group_attribute', metadata,
    Column('groupid', ForeignKey('tc_groups.id', ondelete='CASCADE'), nullable=False),
    Column('attributeid', ForeignKey('tc_attributes.id', ondelete='CASCADE'), nullable=False)
)


t_tc_group_command = Table(
    'tc_group_command', metadata,
    Column('groupid', ForeignKey('tc_groups.id', ondelete='CASCADE'), nullable=False),
    Column('commandid', ForeignKey('tc_commands.id', ondelete='CASCADE'), nullable=False)
)


t_tc_group_driver = Table(
    'tc_group_driver', metadata,
    Column('groupid', ForeignKey('tc_groups.id', ondelete='CASCADE'), nullable=False),
    Column('driverid', ForeignKey('tc_drivers.id', ondelete='CASCADE'), nullable=False)
)


t_tc_group_maintenance = Table(
    'tc_group_maintenance', metadata,
    Column('groupid', ForeignKey('tc_groups.id', ondelete='CASCADE'), nullable=False),
    Column('maintenanceid', ForeignKey('tc_maintenances.id', ondelete='CASCADE'), nullable=False)
)


class TcNotification(Base):
    __tablename__ = 'tc_notifications'

    id = Column(Integer, primary_key=True)
    type = Column(String(128), nullable=False)
    attributes = Column(String(4000))
    always = Column(Boolean, nullable=False, server_default=text("false"))
    calendarid = Column(ForeignKey('tc_calendars.id', ondelete='SET NULL', onupdate='RESTRICT'))
    notificators = Column(String(128))

    tc_calendar = relationship('TcCalendar')
    tc_users = relationship('TcUser', secondary='tc_user_notification')


t_tc_user_attribute = Table(
    'tc_user_attribute', metadata,
    Column('userid', ForeignKey('tc_users.id', ondelete='CASCADE'), nullable=False),
    Column('attributeid', ForeignKey('tc_attributes.id', ondelete='CASCADE'), nullable=False)
)


t_tc_user_calendar = Table(
    'tc_user_calendar', metadata,
    Column('userid', ForeignKey('tc_users.id', ondelete='CASCADE'), nullable=False),
    Column('calendarid', ForeignKey('tc_calendars.id', ondelete='CASCADE'), nullable=False)
)


t_tc_user_command = Table(
    'tc_user_command', metadata,
    Column('userid', ForeignKey('tc_users.id', ondelete='CASCADE'), nullable=False),
    Column('commandid', ForeignKey('tc_commands.id', ondelete='CASCADE'), nullable=False)
)


t_tc_user_driver = Table(
    'tc_user_driver', metadata,
    Column('userid', ForeignKey('tc_users.id', ondelete='CASCADE'), nullable=False),
    Column('driverid', ForeignKey('tc_drivers.id', ondelete='CASCADE'), nullable=False)
)


t_tc_user_group = Table(
    'tc_user_group', metadata,
    Column('userid', ForeignKey('tc_users.id', ondelete='CASCADE'), nullable=False),
    Column('groupid', ForeignKey('tc_groups.id', ondelete='CASCADE'), nullable=False)
)


t_tc_user_maintenance = Table(
    'tc_user_maintenance', metadata,
    Column('userid', ForeignKey('tc_users.id', ondelete='CASCADE'), nullable=False),
    Column('maintenanceid', ForeignKey('tc_maintenances.id', ondelete='CASCADE'), nullable=False)
)


t_tc_user_user = Table(
    'tc_user_user', metadata,
    Column('userid', ForeignKey('tc_users.id', ondelete='CASCADE'), nullable=False),
    Column('manageduserid', ForeignKey('tc_users.id', ondelete='CASCADE'), nullable=False)
)


t_tc_device_attribute = Table(
    'tc_device_attribute', metadata,
    Column('deviceid', ForeignKey('tc_devices.id', ondelete='CASCADE'), nullable=False),
    Column('attributeid', ForeignKey('tc_attributes.id', ondelete='CASCADE'), nullable=False)
)


t_tc_device_command = Table(
    'tc_device_command', metadata,
    Column('deviceid', ForeignKey('tc_devices.id', ondelete='CASCADE'), nullable=False),
    Column('commandid', ForeignKey('tc_commands.id', ondelete='CASCADE'), nullable=False)
)


t_tc_device_driver = Table(
    'tc_device_driver', metadata,
    Column('deviceid', ForeignKey('tc_devices.id', ondelete='CASCADE'), nullable=False),
    Column('driverid', ForeignKey('tc_drivers.id', ondelete='CASCADE'), nullable=False)
)


t_tc_device_geofence = Table(
    'tc_device_geofence', metadata,
    Column('deviceid', ForeignKey('tc_devices.id', ondelete='CASCADE'), nullable=False),
    Column('geofenceid', ForeignKey('tc_geofences.id', ondelete='CASCADE'), nullable=False)
)


t_tc_device_maintenance = Table(
    'tc_device_maintenance', metadata,
    Column('deviceid', ForeignKey('tc_devices.id', ondelete='CASCADE'), nullable=False),
    Column('maintenanceid', ForeignKey('tc_maintenances.id', ondelete='CASCADE'), nullable=False)
)


t_tc_device_notification = Table(
    'tc_device_notification', metadata,
    Column('deviceid', ForeignKey('tc_devices.id', ondelete='CASCADE'), nullable=False),
    Column('notificationid', ForeignKey('tc_notifications.id', ondelete='CASCADE'), nullable=False)
)


class TcEvent(Base):
    __tablename__ = 'tc_events'
    __table_args__ = (
        Index('event_deviceid_servertime', 'deviceid', 'servertime'),
    )

    id = Column(Integer, primary_key=True)
    type = Column(String(128), nullable=False)
    servertime = Column(DateTime, nullable=False)
    deviceid = Column(ForeignKey('tc_devices.id', ondelete='CASCADE'))
    positionid = Column(Integer)
    geofenceid = Column(Integer)
    attributes = Column(String(4000))
    maintenanceid = Column(Integer)

    tc_device = relationship('TcDevice')


t_tc_group_geofence = Table(
    'tc_group_geofence', metadata,
    Column('groupid', ForeignKey('tc_groups.id', ondelete='CASCADE'), nullable=False),
    Column('geofenceid', ForeignKey('tc_geofences.id', ondelete='CASCADE'), nullable=False)
)


t_tc_group_notification = Table(
    'tc_group_notification', metadata,
    Column('groupid', ForeignKey('tc_groups.id', ondelete='CASCADE'), nullable=False),
    Column('notificationid', ForeignKey('tc_notifications.id', ondelete='CASCADE'), nullable=False)
)


class TcPosition(Base):
    __tablename__ = 'tc_positions'
    __table_args__ = (
        Index('position_deviceid_fixtime', 'deviceid', 'fixtime'),
    )

    id = Column(Integer, primary_key=True)
    protocol = Column(String(128))
    deviceid = Column(ForeignKey('tc_devices.id', ondelete='CASCADE'), nullable=False)
    servertime = Column(DateTime, nullable=False, server_default=text("now()"))
    devicetime = Column(DateTime, nullable=False)
    fixtime = Column(DateTime, nullable=False)
    valid = Column(Boolean, nullable=False)
    latitude = Column(Float(53), nullable=False)
    longitude = Column(Float(53), nullable=False)
    altitude = Column(Float(53), nullable=False)
    speed = Column(Float(53), nullable=False)
    course = Column(Float(53), nullable=False)
    address = Column(String(512))
    attributes = Column(String(4000))
    accuracy = Column(Float(53), nullable=False, server_default=text("0"))
    network = Column(String(4000))

    tc_device = relationship('TcDevice')


t_tc_user_device = Table(
    'tc_user_device', metadata,
    Column('userid', ForeignKey('tc_users.id', ondelete='CASCADE'), nullable=False, index=True),
    Column('deviceid', ForeignKey('tc_devices.id', ondelete='CASCADE'), nullable=False)
)


t_tc_user_geofence = Table(
    'tc_user_geofence', metadata,
    Column('userid', ForeignKey('tc_users.id', ondelete='CASCADE'), nullable=False),
    Column('geofenceid', ForeignKey('tc_geofences.id', ondelete='CASCADE'), nullable=False)
)


t_tc_user_notification = Table(
    'tc_user_notification', metadata,
    Column('userid', ForeignKey('tc_users.id', ondelete='CASCADE'), nullable=False),
    Column('notificationid', ForeignKey('tc_notifications.id', ondelete='CASCADE'), nullable=False)
)
