from s_models import TcDevice
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
import json


def main():
    Session = sessionmaker()
    engine = create_engine("postgresql://postgres:dblab4458@db.hanyang.ac.kr:54321/traccar")
    Session.configure(bind=engine)

    sess = Session()

    query = sess.query(TcDevice).filter(TcDevice.id == 328).all()

    def volunteer_to_user(row):
        attr = json.loads(row.attributes)
        print(type(attr), attr)
        print(attr.get("volunteerId"), attr.get("volunteerName"))
        if attr.get("volunteerId"):
            attr["userId"] = attr.get("volunteerId")
        if attr.get("volunteerName"):
            attr["userName"] = attr.get("volunteerName")
        print(attr)
        row.attributes = json.dumps(attr)

    # for row in query:
    #     volunteer_to_user(row)

    print(query[0])

    def get_user_name(row):
        attr = json.loads(row.attributes)
        print(attr.get("volunteerName"))

    get_user_name(query[0])

    sess.commit()


if __name__ == "__main__":
    main()
