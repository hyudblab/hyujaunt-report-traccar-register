import os
from server import create_app


app = create_app()

if __name__ == "__main__":
    mode = os.getenv("FLASK_ENV")
    if "dev" in mode:
        app.run(host="0.0.0.0", debug=True)
    else:
        print("WARNING!: FLASK_ENV environment Found. Are you sure to run in current environment?")
        app.run(host="0.0.0.0")
