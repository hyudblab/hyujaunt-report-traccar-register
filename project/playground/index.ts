import { Client } from 'pg'


async function main() {
  const client = new Client("postgresql://postgres:dblab4458@db.hanyang.ac.kr:54321/traccar")
  await client.connect()

  const data = await client.query("SELECT * FROM tc_devices where id= 51")
  console.log(Object.keys(data))
  const attr = data.rows[0].attributes
  console.log(typeof attr)
  console.log(JSON.parse(attr))

  await client.end()
}

main()
